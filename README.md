# Airweb - Test Technique développeur front-end - Vincent Carpentier

Bonjour !

Voici le retour du petit test permettant de juger mes capacités et compétences techniques.

Ce projet a été réalisé avec le framework Angular et la librairie graphique Angular Material.

## Instructions pour lancer le serveur web

- Tout d'abord, veuillez cloner le projet dans le répertoire de votre choix :

```bash
$ git clone https://gitlab.com/BiggyDev/test-technique-airweb.git
```

- Si Angular n'est pas encore installé sur votre machine, vous pouvez utiliser cette commande :

```bash
$ npm install -g angular/cli@latest
```

- Naviguez au sein du répertoire du projet et installez les dépendances nécessaires à son bon fonctionnement :

```bash
$ cd test-technique-airweb
$ npm install
```

- Lancer le serveur web :

```bash
$ ng serve
```

Le serveur web devrait ensuite être disponible à l'adresse : [localhost:4200](http://localhost:4200)

## Localize and Internationalization (**i18n**)

Pour lancer le projet dans la deuxième langue configurée, il est nécessaire de build le projet grâce à la commande suivante :

```bash
$ ng build --localize
```

Cette commande va créer un dossier dist à la racine du projet, contenant deux autres dossiers :
- Le dossier avec la langue de base, ici **fr-CA**
- Un autre dossier avec la langue de traduction choisie, ici **en-US**

Pour lancer le serveur avec la langue traduite, il faut effectuer les commandes suivantes :

```bash
$ cd dist/test-technique-airweb/en-US
$ ng serve
```

Le serveur web dans la langue traduite devrait ensuite être disponible à l'adresse : [localhost:4200](http://localhost:4200)

**NB : Seuls les éléments de texte statiques ont été traduits.**

## A propos des fonctionnalités

Sur ce projet, il est possible de :

- Visualiser l'ensemble des produits mis à disposition dans l'API sous forme de liste
- Ajouter un ou plusieurs produits dans le panier
- Filtrer l'affichage des produits de la liste par rapport aux catégories de chaque produit
- Visualiser le panier sous forme de liste de produits
- Pouvoir supprimer un des produits du panier directement depuis la ligne du tableau correspondante
- Voir le montant total du panier
- Visualiser le nombre de produits présents dans le panier directement depuis la navbar

Je reste à votre disposition pour davantage de renseignements sur le projet réalisé. A très vite.
