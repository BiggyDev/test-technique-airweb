import { Component, OnInit } from '@angular/core';
import {ProductService} from "../services/product.service";
import {Product} from "../models/Product";
import {Category} from "../models/Category";
import {CategoryService} from "../services/category.service";
import {CartService} from "../services/cart.service";
import {CartItem} from "../models/CartItem";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.sass']
})
export class ProductListComponent implements OnInit {
  products: Product[] = [];
  categories: Category[] = [];
  displayedProducts: Product[] = [];
  cart: CartItem[] = [];

  constructor(private productService: ProductService,
              private categoryService: CategoryService,
              private cartService: CartService,
              private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.getAllProducts();
    this.getAllCategories();
    this.cartService.getProductInCart();
  }

  getAllProducts(): void {
    this.productService.getAllProducts().subscribe((products) => {
      this.products = products;
      this.displayedProducts = products;
    })
  }

  getAllCategories(): void {
    this.categoryService.getAllCategories().subscribe((categories) => {
      this.categories = categories;
    })
  }

  filterByCategory(id: string): void {
    this.displayedProducts = this.products;
    this.displayedProducts = this.displayedProducts.filter(product => product.category_id === id);
  }

  resetFiltering(): void {
    this.displayedProducts = this.products;
  }

  addProductToCart(product: Product): void {
    this.cartService.addProductToCart(product);
    this.openAddedProductToCartSnackBar(product.label);
  }

  openAddedProductToCartSnackBar(productName: string) {
    this._snackBar.open(productName + ' ajouté au panier', 'OK');
  }

}
