export class Product {
  id: number;
  label: string;
  description: string;
  price: number;
  category_id: string;
  thumbnail_url: string | null;


  constructor(id: number, label: string, description: string, price: number, category_id: string, thumbnail_url: string | null) {
    this.id = id;
    this.label = label;
    this.description = description;
    this.price = price;
    this.category_id = category_id;
    this.thumbnail_url = thumbnail_url;
  }
}
