export class Category {
  id: number
  index: number;
  label: string;
  description: string;


  constructor(id: number, index: number, label: string, description: string) {
    this.id = id;
    this.index = index;
    this.label = label;
    this.description = description;
  }
}
