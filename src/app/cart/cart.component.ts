import { Component, OnInit } from '@angular/core';
import {CartService} from "../services/cart.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {CartItem} from "../models/CartItem";
import {DataSource} from "@angular/cdk/collections";
import {Observable, ReplaySubject} from "rxjs";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.sass']
})
export class CartComponent implements OnInit {
  cart: CartItem[] = [];
  displayedColumns: string[] = ['Produit', 'Quantité', 'Prix', 'Actions'];
  dataSource = new CartDataSource([]);

  constructor(private cartService: CartService,
              private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.cart = this.getProductsInCart();
    this.dataSource = new CartDataSource(this.cart);
  }

  getProductsInCart(): CartItem[] {
    return this.cartService.getProductInCart()
  }

  removeProductFromCart(cartItem: CartItem): void {
    this.cartService.removeProductFromCart(cartItem);
    return this.dataSource.setData(this.cart);
  }

  openCartValidationSnackBar() {
    this._snackBar.open('Commande validée', 'OK');
  }

  getTotalCost() {
    return this.cart.map(p => p.product.price).reduce((acc, value) => acc + value, 0);
  }

}

class CartDataSource extends DataSource<CartItem> {
  private _dataStream = new ReplaySubject<CartItem[]>();

  constructor(initialData: CartItem[]) {
    super();
    this.setData(initialData);
  }

  connect(): Observable<CartItem[]> {
    return this._dataStream;
  }

  disconnect() {}

  setData(data: CartItem[]) {
    this._dataStream.next(data);
  }
}
