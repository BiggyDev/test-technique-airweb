import { Component, OnInit } from '@angular/core';
import {CartService} from "../services/cart.service";
import {CartItem} from "../models/CartItem";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {
  cart: CartItem[] = [];

  constructor(private cartService: CartService) { }

  ngOnInit(): void {
    this.cart = this.cartService.getProductInCart();
  }

}
