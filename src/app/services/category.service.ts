import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Category} from "../models/Category";

@Injectable({
  providedIn: "root"
})

export class CategoryService {
  baseUrl: string = 'http://localhost:3000/categories';
  httpOptions = {
  headers: new HttpHeaders({
    'Content-type': 'application/json'
    })
  }

  constructor(private http: HttpClient) {}

  getAllCategories(): Observable<Category[]> {
    return this.http.get<Category[]>(this.baseUrl, this.httpOptions);
  }

  getOneCategoryById(id: number): Observable<Category> {
    return this.http.get<Category>(this.baseUrl + `/${id}`, this.httpOptions);
  }
}
