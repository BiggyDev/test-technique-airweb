import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Product} from "../models/Product";

@Injectable({
  providedIn: "root"
})

export class ProductService {
  baseUrl: string = 'http://localhost:3000/products';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-type': 'application/json'
    })
  }

  constructor(private http: HttpClient) {}

  getAllProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this.baseUrl, this.httpOptions);
  }

  getOneProductById(id: number): Observable<Product> {
    return this.http.get<Product>(this.baseUrl + `/${id}`, this.httpOptions);
  }
}
