import {Injectable} from "@angular/core";
import {Product} from "../models/Product";
import {CartItem} from "../models/CartItem";

@Injectable({
  providedIn: "root"
})

export class CartService {
  cart: CartItem[] = [];

  constructor() {}

  addProductToCart(product: Product) {
    if (this.cart.includes({product: product, quantity: 1})) {
      let index = this.cart.indexOf({product: product, quantity: 1});
      this.cart.slice(index, 1);
      return this.cart.push({product: product, quantity: 1});
    } else {
      return this.cart.push({product: product, quantity: 1});
    }
  }

  removeProductFromCart(cartItem: CartItem) {
    let index = this.cart.indexOf(cartItem, 0)
    this.cart.splice(index, 1);
  }

  getProductInCart() {
    return this.cart;
  }

  clearCart() {
    this.cart = [];
    return this.cart;
  }

}
